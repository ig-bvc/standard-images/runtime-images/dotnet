# DVZ Basisimages .NET-Runtime und ASP.NET-Runtime

Die DVZ Basisimages für .NET-Runtime und ASP.NET-Runtime werden auf Debian-Bullseye bereitgestellt.

Diese Basisimage geben nur eine Versionsinformation (dotnet --info) aus.

## Imagedetails

### User:
- www-data

### Port:
- kein Default Port

### Umgebungsvariablen:

- LANG=C

## Verwendung in einem Applikationsimage

Ein Dockerfile für eine Applikation kann nach folgendem Beispiel erstellt werden.

```dockerfile
FROM docker-dev-local.dev.dvz-mv.net/scm/dotnet-runtime:6.0-bullseye
WORKDIR /var/myapp
ADD ./demo/ .
ENV ASPNETCORE_URLS=http://*:8080
EXPOSE 8080
ENTRYPOINT ["dotnet", "aspnet-demo.dll"]
```