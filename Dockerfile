ARG DEBIANCODE=bullseye
FROM registry.opencode.de/ig-bvc/standard-images/grund-images/debian:$DEBIANCODE
ARG DEBIANCODE=bullseye
ARG RUNTIME_VARIANT=dotnet
ARG RUNTIME_VERSION=6.0
ARG HTTP_PROXY
ARG HTTPS_PROXY
ENV DEBIAN_FRONTEND=noninteractive
RUN if ! [ -z HTTP_PROXY ]; then echo "Acquire::http { Proxy \"$HTTP_PROXY\"; };" > /etc/apt/apt.conf.d/02proxy; fi &&\
    if ! [ -z HTTPS_PROXY ]; then echo "Acquire::https { Proxy \"$HTTPS_PROXY\"; };" >> /etc/apt/apt.conf.d/02proxy; fi &&\
    apt-get update &&\
    apt-get install --no-install-recommends --no-install-suggests -y lsb-release apt-transport-https ca-certificates gnupg2 curl &&\
    curl -L https://packages.microsoft.com/keys/microsoft.asc | gpg --no-default-keyring --keyring gnupg-ring:/etc/apt/trusted.gpg.d/microsoft.gpg --import &&\
    chmod 644 /etc/apt/trusted.gpg.d/microsoft.gpg &&\
    echo "deb https://packages.microsoft.com/debian/11/prod $DEBIANCODE main" > /etc/apt/sources.list.d/dotnetrepo.list &&\
    apt-get update && apt-get install --no-install-recommends --no-install-suggests -y $RUNTIME_VARIANT-runtime-$RUNTIME_VERSION &&\
    rm /etc/apt/apt.conf.d/02proxy
RUN dpkg --purge --force-all bash
RUN dpkg-query -W -f='${binary:Package} ${Architecture} ${Version} ${db:Status-Status}\n' |sort > /base_package.list && md5sum base_package.list > base_package.list.md5
USER www-data
ENV LANG=C
ENTRYPOINT ["dotnet", "--info"]